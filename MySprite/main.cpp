#include <windows.h>
#include <string>
#include <cassert>
#include <d3d11.h>
#include <vector>

#include "WindowUtils.h" 
#include "D3DUtil.h"
#include "D3D.h"
#include "CommonStates.h"
#include "SimpleMath.h"
#include "SpriteBatch.h" 
#include "TexCache.h"
#include "Sprite.h"

using namespace std;
using namespace DirectX;
using namespace DirectX::SimpleMath;


/*
*/
class Game
{
public:
	enum class State { PLAY };
	State state = State::PLAY;
	
	Game(MyD3D& d3d);


	void Release();
	void Update(float dTime);
	void Render(float dTime);
private:
	MyD3D& mD3D;
	SpriteBatch *pSB = nullptr;
};






Game::Game(MyD3D& d3d)
	: mD3D(d3d), pSB(nullptr)
{
	pSB = new SpriteBatch(&mD3D.GetDeviceCtx());

	TexCache& cache = mD3D.GetCache();
	cache.LoadTexture(&d3d.GetDevice(), "backdrop.dds");
	cache.LoadTexture(&d3d.GetDevice(), "2dsprite.dds");
	cache.LoadTexture(&d3d.GetDevice(), "ship.dds");
}


//any memory or resources we made need releasing at the end
void Game::Release()
{
	delete pSB;
	pSB = nullptr;
}

//called over and over, use it to update game logic
void Game::Update(float dTime)
{
}

//called over and over, use it to render things
void Game::Render(float dTime)
{
	mD3D.BeginRender(Colours::Black);
	
	CommonStates dxstate(&mD3D.GetDevice());
	pSB->Begin(SpriteSortMode_Deferred, dxstate.NonPremultiplied(),&mD3D.GetWrapSampler());

	Sprite spr(mD3D);
	spr.SetTex(*mD3D.GetCache().Get("backdrop").pTex);
	spr.SetPos(Vector2(10, 10));
	spr.Draw(*pSB);

	spr.SetTex(*mD3D.GetCache().Get("2dsprite").pTex);
	spr.SetPos(Vector2(200, 100));
	spr.Draw(*pSB);

	spr.SetTex(*mD3D.GetCache().Get("ship").pTex);
	spr.SetPos(Vector2(500, 200));
	spr.Draw(*pSB);

	pSB->End();
	mD3D.EndRender();
}

//if ALT+ENTER or resize or drag window we might want do
//something like pause the game perhaps, but we definitely
//need to let D3D know what's happened (OnResize_Default).
void OnResize(int screenWidth, int screenHeight, MyD3D& d3d)
{
	d3d.OnResize_Default(screenWidth, screenHeight);
}

//messages come from windows all the time, should we respond to any specific ones?
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	//do something game specific here
	switch (msg)
	{
		// Respond to a keyboard event.
	case WM_CHAR:
		switch (wParam)
		{
		case 27:
		case 'q':
		case 'Q':
			PostQuitMessage(0);
			return 0;
		}
	}

	//default message handling (resize window, full screen, etc)
	return WinUtil::DefaultMssgHandler(hwnd, msg, wParam, lParam);
}

//main entry point for the game
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{

	int w(1024), h(512);
	//int defaults[] = { 640,480, 800,600, 1024,768, 1280,1024 };
		//WinUtil::ChooseRes(w, h, defaults, 4);
	if (!WinUtil::Get().InitMainWindow(w, h, hInstance, "Fezzy", MainWndProc, true))
		assert(false);

	MyD3D d3d;
	if (!d3d.InitDirect3D(OnResize))
		assert(false);
	WinUtil::Get().SetD3D(d3d);
	d3d.GetCache().SetAssetPath("data/");
	Game game(d3d);

	bool canUpdateRender;
	float dTime = 0;
	while (WinUtil::Get().BeginLoop(canUpdateRender))
	{
		if (canUpdateRender)
		{
			game.Update(dTime);
			game.Render(dTime);
		}
		dTime = WinUtil::Get().EndLoop(canUpdateRender);
	}

	game.Release();
	#if _DEBUG
	d3d.ReleaseD3D(true);	
	#else
	d3d.ReleaseD3D(false);
	#endif
	return 0;
}

